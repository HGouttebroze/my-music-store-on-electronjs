const { ipcRenderer } = require("electron");

function generateRowLine(tableId, tableData) {
  const tbody = document.querySelector(`#${tableId}`);

  tableData.forEach((row) => {
    const tr = document.createElement("tr");

    const thId = document.createElement("th");
    thId.scope = "row";
    thId.innerText = row.id; // id de mes instruments

    const tdLabel = document.createElement("td");
    tdLabel.innerText = row.label; // j'injecte les lignes de mes marques d'instrument ds 1 colonne sur le label
    // que où j'ai créé les données dans un tableau d'objets dans le main principal

    const tdValue = document.createElement("td");
    tdValue.innerText = row.value;

    const tdActions = document.createElement("td");

    const editBtn = document.createElement("button");
    editBtn.classList.add("btn", "btn-warning", "mx-2");
    editBtn.innerText = "Modif.";

    const onClickEditBtn = () => {
      ipcRenderer.invoke("show-confirm-edit-item", {});
    };

    const deleteBtn = document.createElement("button");
    deleteBtn.classList.add("btn", "btn-danger", "mx-2");
    deleteBtn.innerText = "Suppr.";

    // button a ijjerter ci dessus:
    // <button type="submit" class="btn btn-dark mb-2">
    //   Ajouter
    // </button>;

    const onClickDeleteBtn = () => {
      ipcRenderer
        .invoke("show-confirm-delete-item", {
          id: row.id,
          type: tableId.split("-")[0],
        })
        .then((res) => {
          if (res.choice) {
            // 1 is the index in the array buttons but 1 is also true (boolean)
            tr.remove();
            // see error
            updateBalanceSheet(res.instruments);
          }
        });
    };

    deleteBtn.addEventListener("click", onClickDeleteBtn);

    tdActions.append(editBtn, deleteBtn);

    tr.append(thId, tdLabel, tdValue, tdActions);

    tbody.appendChild(tr); // error to debug with msg: Cannot read property 'appendChild' of null
  });
}

function addProducts(divId, tableData) {
  const div = document.querySelector(`#${divId}`);

  tableData.forEach((row) => {
    const tr = document.createElement("tr");

    const thId = document.createElement("th");
    thId.scope = "row";
    thId.innerText = row.id; // id de mes instruments

    const addBtn = document.createElement("button");
    addBtn.classList.add("btn", "btn-dark", "mb-2");
    addBtn.innerText = "Ajouter instrument";

    tr.append(addBtn);

    div.appendChild(addBtn);
  });
}

////////////////////// INIT DATA //////////////////////
ipcRenderer.once("init-data", (e, data) => {
  console.log(data);

  generateRowLine("instruments-table", data.instruments);
  addProducts("instruments-add", data.instruments);
}); // enpeche la regénération

////////////////////// ADD ITEM //////////////////////
function onClickAddItemBtn(e) {
  // e or evt or event. Send by the click event
  ipcRenderer.send("open-new-item-window", {
    type: e.target.id.split("-")[1], // #add-expense or #add-profit
  });
}

const addInstrumentBtn = document.querySelector("#instruments-add");

addInstrumentBtn.addEventListener("click", onClickAddItemBtn);

////////////////////////// NEW ITEM ADDED LISTENER ////////
ipcRenderer.on("new-item-added", (e, data) => {
  // generateRowLine(`${data.type}-table`, data.instruments);
  generateRowLine("instruments-table", data.item);
});
