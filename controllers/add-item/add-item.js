const { ipcRenderer } = require("electron");

const newItemForm = document.querySelector("#new-item-form");
const newItemSubmit = newItemForm.querySelector("#new-item-submit");

const labelInput = newItemForm.querySelector("#item-label");
const valueInput = newItemForm.querySelector("#item-value");

function onInputCheckValue() {
  if (
    labelInput.value !== "" &&
    !isNan(valueInput.value) &&
    valueInput.value > 0
  ) {
    newItemSubmit.hidden = false;
  } else {
    newItemSubmit.hidden = true;
  }
}

labelInput.addEventListener("input", onInputCheckValue);
valueInput.addEventListener("input", onInputCheckValue);

function onSubmitNewItem(e) {
  e.preventDefault();

  const newItem = {
    label: labelInput.value,
    value: valueInput.value,
  };

  ipcRenderer.invoke("new-item", newItem).then((msg) => {
    // invoke: enoie 1 msg & attend 1 réponse (contrairement au send)
    const msgDiv = document.querySelector("#response-message");
    msgDiv.innerText = msg;
    msgDiv.hidden = false;

    // hide msg after 1.5 second
    setTimeout(() => {
      msgDiv.hidden = true;
    }, 1500);

    // Reset all
    e.target.reset(); // target represente le formulaire
    newItemSubmit.hidden = true;
  });
}

newItemForm.addEventListener("submit", onSubmitNewItem);
