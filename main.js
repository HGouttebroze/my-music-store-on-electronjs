const { app, BrowserWindow, ipcMain, dialog } = require("electron");
const Store = require("electron-store"); // importation du store pr la percistance des datas
const path = require("path"); // methode node qui ajoute les / ds l'url

const store = new Store();

const instruments = store.has("instruments") ? store.get("instruments") : [];

// si le Store est vide au lancement de l'appli, on pousse ces données initiales
if (instruments.length === 0) {
  // création d'un tableau contenant les datas de mes instruments, avec 1 id, un label et 1 value
  instruments.push({ id: 1, label: "Gibson", value: "Les Paul" });
  instruments.push({ id: 2, label: "Fender", value: "Telecaster" });
  instruments.push({ id: 3, label: "Fender", value: "Mustang" });

  store.set("instruments", instruments);
}

// 3: add a mainWindow's variable at null, in step3
let mainWindow = null;

// 3: add parameters of new window in function createWindow with viewPath
function createWindow(viewPath, width = 1400, height = 1000) {
  // createWindow est 1 function de callback
  const win = new BrowserWindow({
    width: width,
    height: height,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: true,
      enableRemoteModule: false,
      preload: path.join(__dirname, "preload.js"),
    },
  });

  //win.loadFile(path.join(__dirname, "views/home/home.html")); // POUR ETRE SUR UN CHEMIN ABSOLU: path.join = concatainer les / + "__dirname" = est 1 variable environnement qui renvoie le chemin absolu du repo de travail où on est
  // 3: changes:
  win.loadFile(path.join(__dirname, viewPath));
  return win;

  // step 3. we remove
  // ou on peut mettre 1 listener sur le win
  // win.webContents.on("did-finish-load", () => {
  //   // error
  //   win.webContents.send("init-data", { instruments }); // init data renvoie au controlleur // l'ES6 sur objet littéral des tableau
  // });
}

////////////////////////////////////////////////////////////////////
// 3. to add / change
app.whenReady().then(() => {
  mainWindow = createWindow("views/home/home.html");
  mainWindow.webContents.on("did-finish-load", () => {
    mainWindow.webContents.send("init-data", {
      instruments: instruments,
    });
  });
}); // whenReady est l'equivalent du listen de Node

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

// 3. change
app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    mainWindow = createWindow("views/home/home.html");

    mainWindow.webContents.on("did-finish-load", () => {
      mainWindow.webContents.send("init-data", {
        instruments: instruments,
      });
    });
  }
});

// 3. remove
// app.on("activate", () => {
//   if (BrowserWindow.getAllWindows().length === 0) {
//     createWindow();
//   }
// });

/// STEP 3 //////////////////////////////////
///////////////////// IPC ON/HANDLER /////////////////////
ipcMain.on("open-new-item-window", (e, data) => {
  const win = createWindow("views/add-item/add-item.html", 1000, 500);

  // debug
  ipcMain.handle("new-item", (e, newItem) => {
    newItem.id = 1;

    // const arrayToAdd = data.type === "profit" ? profits : expenses;
    //const arrayToAdd = data.type === "instruments" ? instruments : instruments;

    if (instruments.length > 0) {
      newItem.id = instruments[instruments.length - 1].id + 1;
    }

    instruments.push(newItem);

    mainWindow.webContents.send("new-item-added", {
      //type: data.type,
      item: [newItem], // on pousse l'item
      //instruments: instruments,
    });

    return "Le nouvel instrument a correctement été ajouté!";
  });

  win.on("closed", () => {
    ipcMain.removeHandler("new-item");
  });
});

ipcMain.handle("show-confirm-delete-item", (e, data) => {
  const choice = dialog.showMessageBoxSync({
    type: "warning",
    buttons: ["Non", "Oui"],
    title: "Confirmation de suppression",
    message: "Etes-vous sûr de vouloir supprimer cet instrument?",
  });

  // 1 = true;
  if (choice) {
    const inArrayToSearch =
      data.type === "instruments" ? instruments : instruments;

    for (let [index, item] of inArrayToSearch.entries()) {
      if (item.id === data.id) {
        inArrayToSearch.splice(index, 1);
        break;
      }
    }
  }

  return { choice, instruments };
});
