# Ajout & suppression: mise en place du multi-fenêtrage

Nous allons mettre en place le multi-fenêtrage dans notre application de garage.

Le but du TP va être de basculer le formulaire d'ajout d'une voiture dans une nouvelle fenêtre totalement indépendante.

L'ouverture de la fenêtre se fera à partir d'un bouton sur l'interface qui enverra une demande à NodeJS pour ouvrir une nouvelle fenêtre.

On permettra aussi à l'utilisateur de modifier & supprimer une voiture en ajoutant une colonne "Actions" dans notre tableau. Cette colonne possédera 2 boutons: modifier et supprimer.

Le bouton modifier ouvrira un formulaire pré-rempli avec les champs de la voiture tandis que le bouton supprimer ouvrira une fenêtre avec demande de confirmation de suppression.

Il sera nécessaire de coder la modification et la suppression.

Chaque fenêtre est indépendante et est exécutée dans un environnement clos. Chacune possédera son propre processus de rendu.

On a vu comment communiquer avec une seule fenêtre et le processus principal mais il est possible de faire du multi-fenêtrage avec Electron.

La communication est alors un peu plus difficile à maintenir selon les cas.

## Nouvelle fenêtre 🪟

Il existe 2 manières de créer une nouvelle fenêtre:

- depuis un processus de rendu
- depuis le processus principal

La première possibilité sera souvent utilisée pour ouvrir des modales, des fenêtres temporaires.

La seconde sera celle à privilégier car elle permet de garder la référence de la fenêtre ouverte et d'établir une communication entre les processus.

## BrowserWindow 🪆

Que soit dans un processus de rendu ou dans le processus principal, la création d'une nouvelle fenêtre se fera toujours de la même manière à savoir grâce au module `BrowserWindow`.

Le process de communication se complexifie lorsque l'on souhaite faire une communication triangulaire.

Imaginons une application avec une fenêtre principale et une fenêtre secondaire comme le schéma suivant:

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/367a6caf-4c78-4e01-9a4b-2f388ef50dc1/Capture_decran_2021-03-12_a_13.30.09.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/367a6caf-4c78-4e01-9a4b-2f388ef50dc1/Capture_decran_2021-03-12_a_13.30.09.png)

La fenêtre secondaire possède un formulaire d'ajout. Elle envoie l'information à NodeJS et NodeJS veut envoyer le nouvel élément à la fenêtre principale.

Sauf que jusqu'à présent, nous utilisions la méthode `invoke` pour renvoyer sur la même fenêtre !

Il va donc falloir, dans un schéma comme celui-ci, que le rendu principal stocke de manière globale les références de chaque fenêtre pour ensuite pouvoir accéder à leur propriété `webContents` pour utiliser la méthode `send` dessus.

```jsx
// Imaginons une fonction createWindow qui crée une fenêtre et renvoie la référence
// Dans le du schéma on va ouvrir une fenêtre lorsqu'on recevra un événement
// 'open-new-item-window'
const mainWindow = createWindow();

ipcMain.on("open-new-item-window", (e, data) => {
  // À ce moment là, on va créer la fenêtre secondaire
  const win = createWindow();

  // La fenêtre secondaire enverra le formulaire sur le channel 'new-item'
  ipcMain.handle("new-item", (e, newItem) => {
    // Grâce à la référence de la fenêtre principale, on va pouvoir
    // lui envoyer des informations sans problème
    mainWindow.webContents.send("new-item-added", {
      item: [newItem],
    });

    // On renvoie quand même une réponse à la seconde fenêtre
    return "L'item a correctement été ajouté !";
  });

  // Lorsque la fenêtre secondaire sera fermée, on détruira le handler
  win.on("closed", () => {
    ipcMain.removeHandler("new-item");
  });
});
```

## Boîtes de dialogue 📣

Electron permet d'ouvrir des boites de dialogue avec une UI identique à celle d'une application native.

Il existe différents types de boite de dialogue. Une des plus utilisées est celle pour afficher un message de confirmation.

Les boites de dialogues se trouvent dans le module `dialog` et pour afficher celle avec un message, on utilisera la méthode `showMessageBoxSync`.

On peut passer de nombreux paramètres à cette méthode, [voir la doc](https://www.electronjs.org/docs/api/dialog#dialogshowmessageboxsyncbrowserwindow-options).

Exemple d'utilisation:

```jsx
const { dialog } = require("electron");

// Retourne l'index du bouton cliqué
// 0 for 'Non'
// 1 for 'Oui'
const choice = dialog.showMessageBoxSync({
  type: "warning",
  buttons: ["Non", "Oui"],
  title: "Confirmation de suppression",
  message: "Êtes-vous sûr de vouloir supprimer cet élément ?",
});

console.log(choice);
```
