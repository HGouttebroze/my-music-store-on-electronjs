# commandes

# lancement de l'application

`npm start`

Electron utilise Chromium pour permettre l'affichage des technologies Web.

Chromium permet l'exécution du code dans un environnement clos et isolé, bac à sable ou sandbox.

Il sera cependant possible de communiquer entre les environnements de l'application grâce à des processus spécifiques.

Le processus principal (_main process_) se chargera de gérer la partie proche de l'OS, c'est elle qui est supportée par NodeJS.

Le processus de rendu (_render process_) sera quant à lui disponible dans chacune des vues qui sera ouverte. Lorsque la vue sera détruire le processus le sera aussi.

## Utiliser Electron 🍑

L'utilisation d'Electron dans un projet est simple, comme tout module, il suffit de l'installer avec npm.

```bash
npm install --save electron
```

Il est ensuite nécessaire de modifier le point d'entrée dans le fichier `package.json` ainsi que le script `start`:

```json
{
  "name": "project-name",
  "version": "1.0.0",
  "description": "",
  "main": "main.js", // ici on avait index.js avant
  "scripts": {
    "start": "electron .", // ici on avait certainement rien avant
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "electron": "^12.0.1"
  }
}
```

Enfin, la [documentation officielle](https://electronjs.org/docs/api) est très largement bien faite.

## Structure d'une application Electron 🔬

Une application Electron possède un point d'entrée unique à savoir le fichier `main.js`.

C'est ce fichier qui va contenir le processus principal.

On aura ensuite différents dossiers. Une architecture possible est celle du MVC avec un dossier **_views_** pour les vues d'affichage et un dossier **_controllers_** pour les fichiers JS liés aux vues.

## Hello World 👋

Créer un hello world étape par étape en ligne de commande.

On se positionne dans le dossier dans lequel on va stocker notre projet et on crée le dossier du projet:

```bash
mkdir hello-world
```

On se déplace dedans, on va initialiser le `package.json` et installer le module electron & boostrap:

```bash
cd hello-world
npm init -y
npm install --save electron bootstrap
```

On crée notre `main.js`:

```bash
touch main.js
```

On crée un dossier pour notre unique vue et on fait un fichier `home.html`:

```bash
mkdir -p views/home
touch views/home/home.html
```

On modifie le fichier `package.json`:

```json
{
  "name": "hello-world",
  "version": "1.0.0",
  "description": "",
  "main": "main.js",
  "scripts": {
    "start": "electron .",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "@fortawesome/fontawesome-free": "^5.15.2",
    "bootstrap": "^4.6.0",
    "electron": "^12.0.1"
  }
}
```

On modifie le fichier `views/home/home.html`:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <title>Hello World!</title>
    <meta
      http-equiv="Content-Security-Policy"
      content="script-src 'self' 'unsafe-inline';"
    />
    <link
      href="../../node_modules/bootstrap/dist/css/bootstrap.min.css"
      rel="stylesheet"
    />
  </head>
  <body>
    <div class="container text-center mb-5">
      <h1>Hello World!</h1>
    </div>
  </body>
</html>
```

On modifie le fichier `main.js`:

```jsx
const { app, BrowserWindow } = require("electron");
const path = require("path");

// Fonction qui va être appelée quand l'application sera prête
// Elle va créer une fenêtre d'une certaine taille et
// injecter un fichier HTML dedans
function createWindow() {
  const win = new BrowserWindow({
    width: 1400,
    height: 1200,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  // __dirname permet de renvoyer le chemin absolu du projet
  // pratique une fois en prod pour ne jamais avoir de soucis
  win.loadFile(path.join(__dirname, "views/home/home.html"));
}

// On dit à l'application quand tu es prête, tu crées la fenêtre
app.whenReady().then(createWindow);

// Stuff pour Mac entre autre
app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});
```

Enfin, on lance le projet:

```bash
npm start
```
